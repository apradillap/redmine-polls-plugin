# frozen_string_literal: true

Redmine::Plugin.register :polls do
  name 'Polls plugin'
  author 'Adrián Pradilla'
  description 'This is a "tutorial plugin" for Redmine'
  version '0.0.1'
  url 'https://gitlab.com/apradillap/redmine-polls-plugin.git'
  author_url 'https://gitlab.com/apradillap'

  permission :view_polls, polls: :index
  permission :vote_polls, polls: :vote

  project_module :polls do
    permission :view_polls, polls: :index
    permission :vote_polls, polls: :vote
  end

  require_dependency 'twitter_widget_hook_listener'

  settings default: { 'empty' => true }, partial: 'settings/poll_settings'

  menu :project_menu,
       :polls,
       { controller: 'polls', action: 'index' },
       caption: 'Polls',
       after: :activity,
       param: :project_id

  delete_menu_item :top_menu, :my_page
  delete_menu_item :top_menu, :help
  delete_menu_item :project_menu, :overview
  delete_menu_item :project_menu, :activity
  delete_menu_item :project_menu, :news
end
