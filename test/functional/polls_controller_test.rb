# frozen_string_literal: true

require File.expand_path('../test_helper', __dir__)

class PollsControllerTest < ActionController::TestCase
  fixtures :projects, :users

  def test_index
    get :index, params: {
      project_id: 1
    }

    assert_redirected_to 'http://test.host/login?back_url=http%3A%2F%2Ftest.host%2Fpolls%3Fproject_id%3D1'
  end
end
